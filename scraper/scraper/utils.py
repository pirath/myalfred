import logging
import os
import re

from scrapy import logformatter


def remove_double_space(string):
    return re.sub(' +', ' ', string)
