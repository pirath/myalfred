import scrapy


class PhonePerfSpider(scrapy.Spider):
    name = "phone_perf"
    custom_settings = {
        'ITEM_PIPELINES': {
            'scraper.pipelines.MongoPipeline': 2,
        }
    }

    def start_requests(self):
        urls = [
            'https://benchmarks.ul.com/compare/best-smartphones'
            '?amount=0&sortBy=PERFORMANCE&reverseOrder=true&osFilter=ANDROID,IOS,WINDOWS&test=SLING_SHOT_ES_30'
            '&deviceFilter=PHONE,TABLET,OTHER&displaySize=3.0,15.0',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, )

    def parse(self, response):
        perf = {}
        for tr in response.xpath("//table[@class='navigationtable performancechart']/tbody/tr"):
            perf['name'] = tr.xpath("td[@class='pr1']/a/text()").get()
            perf['rank'] = int(tr.xpath("td[@class='order-cell']/text()").get())
            perf['soc_name'] = tr.xpath(
                "td[@class='list-small-none pr1']/div[@class='list-emphasis']/spin/text()").get()
            description = []
            for spin in tr.xpath("td[@class='list-small-none pr1']/div[@class='list-small-text']/spin"):
                description.append(spin.xpath("text()").get())
            if len(description) == 2:
                [perf['cpu_desc'], perf['gpu_desc']] = [description[0], description[1]]
            else:
                perf['cpu_desc'] = None
                perf['gpu_desc'] = None

            perf['screen_size'] = tr.xpath(
                "td[@class='pr1 relative center list-tiny-none']/div[@class='list-display-size-text']/text()"
            ).get().strip()
            yield perf
