import logging

import scrapy
from scraper.items import Phone
from scraper.utils import remove_double_space
import sys

from scrapy.exceptions import DropItem

sys.path.append("..")
from webapp.utils.string_utils import get_simplified_name as get_slug_name


class LDLCPhonesSpider(scrapy.Spider):
    name = "ldlc_phones"
    custom_settings = {
        'ITEM_PIPELINES': {
            'scrapy.pipelines.images.ImagesPipeline': 1,
            'scraper.pipelines.MongoPipeline': 2,
        }
    }

    def start_requests(self):
        urls = [
            'https://www.ldlc.com/telephonie/telephonie-portable/mobile-smartphone/c4416/+fv991-6515.html',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_ldlc_list)

    def parse_ldlc_list(self, response):
        items = response.xpath("//div[@class='listing-product']/ul/li[@class='pdt-item']")
        items = items[0:5]  # only for tests
        for item in items:
            phone = Phone()
            info = item.xpath("div[@class='dsp-cell-right']/div[@class='pdt-info']")
            desc = info.xpath("div[@class='pdt-desc']/h3")
            phone['desc'] = desc.css("a::text").get()
            phone['ldlc_people_score'] = info.xpath("div[@class='ratingClient']/a/span/@class").get()
            phone['image_urls'] = [item.xpath("div[@class='pic']/a/img/@src").get()]
            yield response.follow(desc.xpath("a/@href").get(),
                                  callback=self.parse_ldlc_item,
                                  meta={'phone': phone})

        next_page_url = response.xpath("//li[@class='next']/a/@href").get()
        if next_page_url:
            yield scrapy.Request('https://www.ldlc.com' + next_page_url,
                                 callback=self.parse_ldlc_list)

    def parse_ldlc_item(self, response):
        params = response.xpath("//table[@id='product-parameters']/tbody")

        def get_text(selector):
            if not selector:
                return None
            text = selector.xpath("a").xpath('string()').get()
            if text:
                return remove_double_space(text.strip())
            else:
                return remove_double_space(selector.xpath('string()').get().strip())

        def get_texts(current_index, count):
            result = []
            for i, tr in enumerate(params.css("tr")):
                if current_index <= i < count + current_index:
                    result.append(get_text(tr.xpath("td[@class='checkbox']")))
            return result[0] if len(result) == 1 else result

        def get_item(item_name):
            for tr_index, tr in enumerate(params.css("tr")):
                label = get_text(tr.xpath("td[@class='label']"))
                if label and item_name in label:
                    item_counter = tr.xpath("td[@class='label']").xpath("@rowspan").get(None)
                    if item_counter:
                        return get_texts(tr_index, int(item_counter))
                    else:
                        try:
                            return get_text(tr.xpath("td[@class='checkbox']"))
                        except TypeError:
                            return get_text(tr.xpath("td[@class='no-checkbox']"))
            return None

        phone = response.request.meta['phone']

        name = get_item('Désignation')
        phone['name'] = name
        phone['s_name'] = get_slug_name(name)
        phone['constructor'] = get_item('Marque')
        phone['model'] = get_item('Modèle')
        phone['dual_sim'] = get_item('Dual SIM')
        phone['sim_format'] = get_item('Format(s) SIM')
        phone['frequency'] = get_item('Fréquence')
        phone['das'] = get_item('DAS')

        screen_size = float(get_item("Taille d'écran").replace("pouces", '').strip())
        phone['screen_size'] = screen_size

        phone['screen_definition'] = get_item("Définition Écran")
        phone['screen_desc'] = get_item('Ecran')

        phone['disk_space'] = get_item('Mémoire')
        phone['expandable_disk'] = get_item('Mémoire extensible')
        phone['ram'] = get_item('Taille mémoire RAM')

        phone['photo'] = get_item('Mode photo')
        phone['photo_3D'] = get_item('Photo 3D')
        phone['photo_resolution'] = get_item('Résolution')

        phone['connectors'] = get_item('Connecteur(s) disponible(s)')

        phone['model_category'] = get_item('Gamme')
        phone['os'] = get_item("Système d'exploitation")
        phone['NFC'] = get_item('NFC')
        phone['fingerprint_reader'] = get_item("Lecteur d'empreintes")

        phone['battery_capacity'] = get_item('Capacité de la batterie')
        phone['battery_fast_charge'] = get_item('Charge rapide')
        phone['battery_induction_compatible'] = get_item('Compatible chargeur à induction')
        phone['battery_removable'] = get_item('Batterie amovible')

        phone['height'] = get_item('Hauteur')
        phone['width'] = get_item('Largeur')
        phone['thickness'] = get_item('Epaisseur')
        phone['weight'] = get_item('Poids')
        phone['waterproof'] = get_item('Étanche')
        phone['shockproof'] = get_item('Antichoc')
        phone['color'] = get_item('Couleur')

        processor = get_item('Processeur')
        phone['proc_name'] = processor
        phone['proc_s_name'] = get_slug_name(processor)
        phone['proc_type'] = get_item('Type de processeur')
        phone['proc_core_count'] = get_item('Nombre de core')
        phone['proc_frequency'] = get_item('Fréquence CPU')

        price = response.xpath("//div[@class='price']/div[@class='price']/text()").get()
        if price:
            price = price.replace('€', '')
            price_sup = response.xpath("//div[@class='price']/div[@class='price']/sup/text()").get()
        else:
            price = response.xpath("//div[@class='price']/div[@class='new-price']/text()").get()
            if not price:
                raise DropItem(f"Cannot add item {phone['name']}. Price is not correctly formed")
            price_sup = response.xpath("//div[@class='price']/div[@class='new-price']/sup/text()").get()

        price = price.replace(' ', '')
        if price_sup:
            phone['price'] = int(price) + int(price_sup) / 100.0
        else:
            phone['price'] = int(price)

        yield phone
