import logging
import random

import pymongo
import scrapy


class PhoneSpecSpider(scrapy.Spider):
    name = "phone_spec"
    custom_settings = {
        'ITEM_PIPELINES': {
            'scrapy.pipelines.images.ImagesPipeline': 1,
            'scraper.pipelines.MongoPipeline': 2,
        }
    }

    SETTINGS = scrapy.utils.project.get_project_settings()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        client = pymongo.MongoClient(self.SETTINGS.get('MONGO_URI'))
        db = client[self.SETTINGS.get('MONGO_DB_NAME')]
        self.added_phones = db[self.name].find({}, {'brand-name': 1, 'name': 1, '_id': 0})

    def start_requests(self):
        yield scrapy.Request(url='https://www.devicespecifications.com/en/brand-more',
                             callback=self.parse_brand_list,
                             )

    def parse_brand_list(self, response):
        for link in response.xpath("//div[@class='brand-listing-container-news']/a"):
            href = link.xpath("@href").get()
            brand_name = link.xpath("text()").get().strip()
            yield scrapy.Request(href,
                                 callback=self.parse_brand,
                                 headers={'User-Agent': random.choice(self.SETTINGS.get('USER_AGENTS'))},
                                 meta={'brand-name': brand_name})

    def parse_brand(self, response):
        phones = []
        for model in response.xpath("//div[@class='model-listing-container-80']"):
            [phone_urls, phone_names] = [
                model.xpath("div/h3/a/@href").extract(),
                model.xpath("div/h3/a/text()").extract()
            ]
            for index, url in enumerate(phone_urls):
                phones.append({
                    'name': phone_names[index].strip(),
                    'url': url,
                    'brand-name': response.request.meta['brand-name'],
                })

        for phone in phones:
            if {'name': phone['name'], 'brand-name': phone['brand-name']} not in self.added_phones:
                yield scrapy.Request(phone['url'],
                                     callback=self.parse_phone,
                                     meta={'phone': phone},
                                     headers={'User-Agent': random.choice(self.SETTINGS.get('USER_AGENTS'))})
            else:
                logging.info(f"phone {phone['name']} already in db")

    def parse_phone(self, response):
        phone = response.request.meta['phone']
        phone['image_urls'] = [
            response.xpath("//div[@id='model-image']/@style")
                .get()
                .replace("background-image: url(", "")
                .replace(")", '')
        ]

        header_sections = response.xpath("//header[@class='section-header']")
        for header in header_sections:
            if header.xpath("following::table"):
                table = header.xpath("following::table")[0]
                section_name = header.xpath("h2/text()").get()
                for tr in table.xpath("tr"):
                    [raw_key, raw_value] = tr.xpath("td")
                    key = raw_key.xpath("text()").extract()
                    values = raw_value.xpath("text()").extract()
                    values = [v.strip() for v in values if v]
                    if key:
                        if section_name not in phone:
                            phone[section_name] = {}
                        phone[section_name].update({
                            key[0]: values[0] if values and len(values) == 1 else values
                        })
        yield phone
