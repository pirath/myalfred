# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
import scrapy


class MongoPipeline(object):
    db = None
    client = None
    SETTINGS = scrapy.utils.project.get_project_settings()

    def __init__(self):
        self.mongo_uri = self.SETTINGS.get('MONGO_URI')
        self.mongo_db = self.SETTINGS.get('MONGO_DB_NAME')

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        self.db[spider.name].insert_one(dict(item))
        return item
