# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class Phone(scrapy.Item):
    proc_s_name = scrapy.Field()
    proc_name = scrapy.Field()
    proc_type = scrapy.Field()
    proc_core_count = scrapy.Field()
    proc_frequency = scrapy.Field()
    proc_power_score = scrapy.Field()

    desc = scrapy.Field()
    ldlc_people_score = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
    price = scrapy.Field()
    name = scrapy.Field()
    s_name = scrapy.Field()
    constructor = scrapy.Field()
    model = scrapy.Field()
    dual_sim = scrapy.Field()
    sim_format = scrapy.Field()
    frequency = scrapy.Field()
    das = scrapy.Field()
    screen_size = scrapy.Field()
    screen_definition = scrapy.Field()
    screen_desc = scrapy.Field()
    disk_space = scrapy.Field()
    expandable_disk = scrapy.Field()
    ram = scrapy.Field()
    photo = scrapy.Field()
    photo_3D = scrapy.Field()
    photo_resolution = scrapy.Field()
    connectors = scrapy.Field()
    model_category = scrapy.Field()
    os = scrapy.Field()
    NFC = scrapy.Field()
    fingerprint_reader = scrapy.Field()
    battery_capacity = scrapy.Field()
    battery_fast_charge = scrapy.Field()
    battery_induction_compatible = scrapy.Field()
    battery_removable = scrapy.Field()
    height = scrapy.Field()
    width = scrapy.Field()
    thickness = scrapy.Field()
    weight = scrapy.Field()
    waterproof = scrapy.Field()
    shockproof = scrapy.Field()
    color = scrapy.Field()
