import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    MONGO_URI = os.environ.get('DATABASE_URL') or "mongodb://127.0.0.1:27017/my-alfred"
    MONGO_DBNAME = "my-alfred"
    SECRET_KEY = os.environ.get('SECRET_KEY') or "4b922b20-ba3a-401f-944b-bb0ffd3ec2c1"

    # Babel env
    LANGUAGES = ['en', 'fr']


class TestConfig(Config):
    TESTING = True
