import os

from webapp import app


if __name__ == '__main__':
    os.environ["DEBUG"] = "true"
    os.environ["TESTING"] = "false"
    os.environ["TEMPLATES_AUTO_RELOAD"] = "true"
    os.environ["ENVIRONMENT"] = "development"
    os.environ["URL"] = 'https//localhost:5007/'
    os.environ["SSL_VERIFICATION"] = "false"

    from webapp import app

    app.run(debug=True, host='localhost', port=5007, threaded=True)
