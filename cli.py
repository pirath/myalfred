import os

import click

from webapp import app


@app.cli.command("test")
def run_tests():
    test_results = os.system(
        'coverage run --source=webapp --omit="*/tests*" -m unittest discover'
    )
    if not test_results:
        if os.system('coverage report'):
            raise RuntimeError('extract command failed')
        if os.system('coverage html'):
            raise RuntimeError('extract command failed')


@app.cli.group()
def db():
    pass


@db.command()
@click.argument('dump_collection')
def dump_collections(collection=None):
    if os.system(f'mongodump  --db=my-alfred --collection={collection}'):
        raise RuntimeError('dump failed')


@app.cli.group()
def translate():
    """Translation and localization commands with pybabel."""
    pass


@translate.command()
@click.argument('lang')
def init(lang):
    """Initialize a new language."""
    if os.system('pybabel extract -F babel.cfg -k _l -o messages.pot .'):
        raise RuntimeError('extract command failed')
    if os.system(
            'pybabel init -i messages.pot -d webapp/translations -l ' + lang):
        raise RuntimeError('init command failed')
    os.remove('messages.pot')


@translate.command()
def pybabel_update():
    """Update all languages."""
    if os.system('pybabel extract -F babel.cfg -k _l -o messages.pot .'):
        raise RuntimeError('extract command failed')
    if os.system('pybabel update -i messages.pot -d webapp/translations'):
        raise RuntimeError('update command failed')
    os.remove('messages.pot')


@translate.command()
def pybabel_compile():
    """Compile all languages."""
    if os.system('pybabel compile -d app/translations'):
        raise RuntimeError('compile command failed')
