# coding: utf-8
import logging
import os

from flask import render_template, request

from webapp import app, babel, db, database

import cli


def initialize():
    database.initialize()
    _initialize_jinja_env()
    print("Server initialized")


def _initialize_jinja_env():
    app.jinja_env.filters['path_join'] = jinja_os_path


def jinja_os_path(path):
    return os.path.join(*path.split('/'))


@app.shell_context_processor
def make_shell_context():
    return {'db': db, }  # pragma: no cover


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(app.config['LANGUAGES'])


@app.route("/")
def home():
    return render_template("home.html")


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404
