from webapp import app
from config import TestConfig
import unittest


class TestServer(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config.from_object(TestConfig)
        self.app = app.test_client()

    def test_not_found_error(self):
        response = self.app.get('/url-who-does-not-exists')
        self.assertEqual(response.status_code, 404)
