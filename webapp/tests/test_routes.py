import json
import unittest

from pymongo import MongoClient

from config import TestConfig
from webapp import app, database
from webapp.tests.data.phone_sample import phone_test1
from webapp.utils.string_utils import JSONDBEncoder


class TestRoutes(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config.from_object(TestConfig)
        self.app = app.test_client()
        client = MongoClient("mongodb://127.0.0.1:27017/myalfred-test")
        database.db = client["myalfred-test"]

    def tearDown(self):
        database.db.drop_collection(database.PHONES_COL_NAME)
        database.db.drop_collection(database.CPU_COL_NAME)

    def test_home_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_get_smartphones(self):
        database.create_phone(phone_test1)
        response = self.app.get('/smartphones')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(phone_test1['name'] in response.data.decode('utf-8'))

    def test_get_smartphone(self):
        database.create_phone(phone_test1)
        phone = database.get_phone(phone_test1['s_name'])
        response = self.app.get('/smartphones/' + str(phone['s_name']))
        self.assertEqual(response.status_code, 200)
        phone = json.dumps(phone, cls=JSONDBEncoder)
        self.assertEqual(
            phone,
            response.data.decode('utf-8')
        )
