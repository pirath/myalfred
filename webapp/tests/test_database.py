import unittest

from pymongo import MongoClient
from webapp import database
from webapp.tests.data.phone_sample import *


class MongoDatabaseTests(unittest.TestCase):

    def setUp(self):
        client = MongoClient("mongodb://127.0.0.1:27017/myalfred-test")
        database.db = client["myalfred-test"]

    def tearDown(self):
        database.db.drop_collection(database.PHONES_COL_NAME)
        database.db.drop_collection(database.CPU_COL_NAME)

    def test__get_s_names(self):
        database.create_phone(phone_test1)
        database.create_phone(phone_test2)
        database.create_phone(phone_test3)
        database._create_cpu({'s_name': 's1'})
        database._create_cpu({'s_name': 's2'})
        database._create_cpu({'s_name': 's3'})
        self.assertEqual(database._get_s_names(database.CPU_COL_NAME),
                         ['s1', 's2', 's3'])

        self.assertEqual(database._get_s_names(database.PHONES_COL_NAME),
                         [phone_test1['s_name'], phone_test2['s_name'], phone_test3['s_name']])

    def test_get_number_of_pages(self):
        per_page = 10
        database.create_phone(phone_test1)
        self.assertEqual(database.get_number_of_pages(database.PHONES_COL_NAME, per_page), 1)

        for i in range(per_page):
            database._create_cpu({'s_name': f"phone-{i}", 'rank': i, 'proc_s_name': f"phone-{i}"})
            database.create_phone({'s_name': f"phone-{i}", 'proc_s_name': f"phone-{i}"})
        self.assertEqual(database.get_number_of_pages(database.PHONES_COL_NAME, per_page), 2)
        for i in range(per_page):
            database.create_phone({'s_name': f"phone-no-rank-{i}", 'rank': None,
                                   'proc_s_name': f"phone-{i}"})
        self.assertEqual(database.get_number_of_pages(database.PHONES_COL_NAME, per_page), 3)
        self.assertEqual(database.get_number_of_pages(
            database.PHONES_COL_NAME,
            per_page,
            unaccepted_none_key='rank'), 2)

    def test_create_phone(self):
        database.create_phone({'name': "Name test", 's_name': "name-test"})
        phone = database.db[database.PHONES_COL_NAME].find_one({'s_name': "name-test"})
        self.assertTrue('date' in phone)
        with self.assertRaises(database.AlreadyExists):
            database.create_phone(phone)
        phone['forbidden-key'] = "forbidden"
        with self.assertRaises(database.IntegrityError):
            database.create_phone(phone)

    def test__update_phone(self):
        database.create_phone({'name': "Name test", 's_name': "name-test"})
        phone = database.get_phone('name-test')
        phone['rank'] = 10
        database._update_phone(phone)
        self.assertEqual(database.get_phone("name-test")['name'], "Name test")
        self.assertEqual(database.get_phone("name-test")['rank'], 10)

    def test_get_phone(self):
        database.create_phone(phone_test1)
        phone = database.db[database.PHONES_COL_NAME].find_one({
            's_name': phone_test1['s_name']
        })
        self.assertEqual(phone, database.get_phone(phone['s_name']))
        with self.assertRaises(database.NotFoundException):
            database.get_phone("name-not-in-db")

    def test_update_or_create(self):
        database.update_or_create(phone_test1)
        self.assertEqual(database.db[database.PHONES_COL_NAME].estimated_document_count(), 1)
        database.update_or_create(phone_test1)
        self.assertEqual(database.db[database.PHONES_COL_NAME].estimated_document_count(), 1)
        database.update_or_create(phone_test2)
        self.assertTrue(database.db[database.PHONES_COL_NAME].estimated_document_count(), 2)

    def test_get_phones(self):
        database.create_phone(phone_test1)
        database.create_phone(phone_test2)
        database.create_phone(phone_test3)
        phones = database.get_phones()
        self.assertEqual(len(phones), 3)
        database.create_phone({'s_name': "none-rank", 'proc_s_name': 'proc-0'})
        self.assertEqual(len(database.get_phones()), 3)

    def test__create_cpu(self):
        cpu = {'s_name': "test", 'rank': 1}
        database._create_cpu(cpu)
        self.assertEqual(database.db[database.CPU_COL_NAME].estimated_document_count(), 1)
        with self.assertRaises(database.AlreadyExists):
            database._create_cpu(cpu)

    def test__get_cpu(self):
        cpu = {'s_name': "test", 'rank': 1}
        database._create_cpu(cpu)
        self.assertEqual(
            database.db[database.CPU_COL_NAME].find_one({'s_name': "test"}),
            database._get_cpu(cpu)
        )

    def test__get_phone_rank(self):
        database._create_cpu({'s_name': 'phone-test', 'rank': 10, 'proc_s_name': 'proc-0'})
        database._create_cpu({'s_name': 'phone-test-pro-plus', 'rank': 5, 'proc_s_name': 'proc-1'})

        database.create_phone({'s_name': "phone-test-1", 'name': "Phone t1", 'proc_s_name': 'proc-0'})
        database.create_phone({'s_name': "phone-test-2", 'name': "Phone t2", 'proc_s_name': 'proc-1'})

        self.assertEqual(database._get_phone_rank(database.get_phone("phone-test-1")), 10)
        self.assertEqual(database._get_phone_rank({'s_name': "phone", 'proc_s_name': 'proc-0'}), 10)
        self.assertEqual(database._get_phone_rank({'s_name': "phone-test-2", 'proc_s_name': 'proc-1'}), None)
        self.assertEqual(database._get_phone_rank({'s_name': 'phone-test-pro-plus', 'proc_s_name': 'proc-0'}), 5)

    def test_update_or_create_cpu(self):
        database.update_or_create_cpu({'s_name': "phone-test-1", 'rank': 5})
        self.assertEqual(database.db[database.CPU_COL_NAME].estimated_document_count(), 1)
        database.update_or_create_cpu({'s_name': "phone-test-1", 'rank': 3})
        self.assertEqual(database.db[database.CPU_COL_NAME].estimated_document_count(), 1)
        self.assertEqual(
            database.db[database.CPU_COL_NAME].find_one({'s_name': 'phone-test-1'})['rank'], 3
        )
        database.update_or_create_cpu({'s_name': "phone-test-2", 'rank': 6})
        self.assertEqual(database.db[database.CPU_COL_NAME].estimated_document_count(), 2)

    def test_update_phones_rank(self):
        database.create_phone(phone_test1)
        database.create_phone(phone_test2)
        database.create_phone(phone_test3)
        s1, s2, s3 = phone_test1['s_name'], phone_test2['s_name'], phone_test3['s_name']
        database._create_cpu({'s_name': s1, 'rank': 1})
        database._create_cpu({'s_name': s2, 'rank': 2})
        database._create_cpu({'s_name': s3, 'rank': 3})
        database.update_phones_rank()
        self.assertEqual(database.get_phone(s1)['rank'], 1)
        self.assertEqual(database.get_phone(s2)['rank'], 2)
        self.assertEqual(database.get_phone(s3)['rank'], 3)
