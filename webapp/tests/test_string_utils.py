import datetime
import json
import unittest

from bson import ObjectId

from webapp.utils.string_utils import closest, JSONDBEncoder, _remove_bracket, get_slug_name


class StringUtilsTests(unittest.TestCase):

    def test_closest(self):
        s_list = [
            'nexus-8', 'nexus-8-pro', 'nexus-8-pro-plus',
            'nokia-8', 'nokia-200', 'nokia'
        ]
        self.assertEqual((2, 'nexus-8'), closest('nexus-8-test', s_list))
        self.assertEqual((4, 'nexus-8-pro-plus'), closest('nexus-8-pro-plus-10', s_list))
        self.assertEqual((1, 'nokia'), closest('nokia', s_list))
        self.assertEqual((0, None), closest('not-in-db', s_list))

    def test_json_db_encoder(self):
        data = {
            'int': 1,
            'set': {1, 2},
            'tuple': (1, 2),
            'datetime': datetime.date(2019, 4, 13),
            'timedelta': datetime.timedelta(days=365, hours=5, minutes=10),
            'object_id': ObjectId('123456123456123456123456'),
        }
        self.assertEqual(
            json.loads(json.dumps(data, cls=JSONDBEncoder)),
            {
                'int': 1,
                'set': [1, 2],
                'tuple': [1, 2],
                'datetime': '2019-04-13',
                'timedelta': '365 days, 5:10:00',
                'object_id': '123456123456123456123456'
            }
        )
        self.assertEqual(json.loads(json.dumps({'int': 1}, cls=JSONDBEncoder)),
                         {'int': 1})

    def test_remove_bracket(self):
        self.assertEqual('hello ', _remove_bracket('hello (world)'))
        self.assertEqual(_remove_bracket('test (with (multiple ) brackets'), 'test  brackets')

    def test_get_slug_name(self):
        self.assertEqual(get_slug_name('Phone 1 (KZ3), 32 Go, black'), 'phone-1-32-go-black')
