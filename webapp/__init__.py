from flask import Flask
from flask_babel import Babel
from flask_bootstrap import Bootstrap
from pymongo import MongoClient

from config import Config

# -- Flask init --- #
app = Flask(__name__)
app._static_folder = "static/"
app.config.from_object(Config)

# --- db init --- #
client = MongoClient(app.config['MONGO_URI'])
db = client[app.config['MONGO_DBNAME']]

# --- Babel init --- #
babel = Babel(app)

# --- bootstrap init --- #
bootstrap = Bootstrap(app)

from webapp import server
server.initialize()

from webapp.smartphones import routes
