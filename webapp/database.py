import datetime
import logging
import sys
import traceback

from pymongo import ASCENDING

from webapp import db
from webapp.utils.string_utils import get_simplified_name

PHONES_COL_NAME = "phones"
_PHONES_EXPIRATION_TIMER = 60 * 60 * 24 * 7 * 4  # 4 weeks


class NotFoundException(Exception):
    pass


def update_phones_table_from_scraped_data(raw_phone_col_name, raw_perf_col_name):
    """
    Function called to update the phones collection used by MyAlfred
    :param raw_phone_col_name: phone specs collection with raw data
    :param raw_perf_col_name: phone perf collection with raw data like rank and cpu name
    :return: None
    """

    def slugify_phone(p):
        result = {
            'name': p['name'],
            'brand_name': p['brand-name'],
            'image_url': p['images'][0]['path'],
            'model': p['Brand and model']['Model'],
            'model_alias': p['Brand and model'].get('Model alias', []),
            'colors': p['Design'].get('Colors'),
            'os': p.get('Operating system', {}).get('Operating system (OS)', []),
            'soc': {
                'name': p['System on Chip (SoC)']['SoC'],
                'cpu': {
                    'name': p['System on Chip (SoC)'].get('CPU'),
                    'cores': int(p['System on Chip (SoC)'].get('CPU cores', 0)),
                    'frequency': p['System on Chip (SoC)'].get('CPU frequency'),
                },
                'gpu': p['System on Chip (SoC)'].get('GPU'),
                'ram': {
                    'capacity': p['System on Chip (SoC)'].get('RAM capacity', []),
                },
            },
            'screen': {
                'diagonal': float(p['Display']['Diagonal size'][0].replace(' in', ''))
            },

        }
        return result

    # def get_score(p, perf):
    #     alias_match = False
    #     for model_alias in p['model_alias']:
    #         if model_alias.lower() in perf['name'].lower():
    #             alias_match = True
    #
    #     simplified_name_match = get_simplified_name(p['name']) == get_simplified_name(perf['name'])
    #     if p['soc']['name']:
    #         soc_name_match = p['soc']['name'].lower() == perf['soc_name'].lower()
    #         soc_partial_match = get_simplified_name(p['soc']['name']) == get_simplified_name(perf['soc_name'])
    #     else:
    #         soc_name_match = False
    #         soc_partial_match = False
    #     if p['soc']['gpu']:
    #         gpu_match = p['soc']['gpu'].lower() in perf['gpu_desc'].lower()
    #     else:
    #         gpu_match = False
    #     if p['soc']['cpu']['name']:
    #         cpu_spec_match = p['soc']['cpu']['name'].lower() in perf['cpu_desc'].lower()
    #     else:
    #         cpu_spec_match = False
    #
    #     try:
    #         screen_size_match = float(perf['screen_size'].replace('"', '')) == p['screen']['diagonal']
    #     except ValueError:
    #         screen_size_match = False
    #
    #     score = 0
    #     if simplified_name_match:
    #         score += 30
    #     if alias_match:
    #         score += 60
    #     if soc_name_match:
    #         score += 60
    #     elif soc_partial_match:
    #         score += 30
    #     if gpu_match:
    #         score += 10
    #     if cpu_spec_match:
    #         score += 10
    #     if screen_size_match:
    #         score += 10
    #     return score
    #
    # def get_phone_rank(p):
    #     perf = db[raw_perf_col_name].find_one({'name': p['name']})
    #     if perf:
    #         return perf['rank']
    #
    #     rank_score = 0
    #     matched_perf_name = None
    #     for perf in db[raw_perf_col_name].find():
    #         score = get_score(p, perf)
    #         if score > rank_score:
    #             rank_score = score
    #             matched_perf_name = perf['name']
    #
    #     if rank_score > 30 and matched_perf_name:
    #         p['score'] = score
    #         return db[raw_perf_col_name].find_one({'name': matched_perf_name})['rank']
    #
    #     raise NotFoundException()

    def get_phone_score(p):
        cpu_score = p[soc]

    if db[raw_phone_col_name].estimated_document_count() == 0:
        raise ValueError(f"{raw_phone_col_name} cannot be empty")
    if db[raw_phone_col_name].estimated_document_count() == 0:
        raise ValueError(f"{raw_phone_col_name} cannot be empty")

    inserted_phones = 0
    updated_phones = 0
    dropped_phones = 0

    for phone in db[raw_phone_col_name].find():
        try:
            phone = slugify_phone(phone)
        except KeyError:
            traceback.print_exc(file=sys.stdout)
            logging.error(f"cannot slugify {phone.get('brand-name'), phone.get('name')}")
            dropped_phones += 1
            continue

        try:
            phone['rank'] = get_phone_rank(phone)
        except NotFoundException:
            logging.error(f"cannot find phone rank for phone {phone['brand_name'], phone['name']}")
            dropped_phones += 1
            continue

        db_phone = db[PHONES_COL_NAME].find_one({'name': phone['name'], 'brand_name': phone['brand_name']})
        if not db_phone:
            db[PHONES_COL_NAME].insert_one({'date': datetime.datetime.now(), **phone})
            inserted_phones += 1
        else:
            phone['date'] = datetime.datetime.now()
            db[PHONES_COL_NAME].update_one({'_id': db_phone['_id']}, {"$set": phone})
            updated_phones += 1
            logging.info(f"phone {phone['brand_name'], phone['name']} already in db")

    return {'insert': inserted_phones, 'update': updated_phones, 'dropped': dropped_phones}


def initialize():
    # db[PHONES_COL_NAME].ensure_index([('name', ASCENDING)], name="name")
    db[PHONES_COL_NAME].ensure_index('date', expireAfterSeconds=_PHONES_EXPIRATION_TIMER)


def _get_s_names(collection):
    return [item['s_name'] for item in db[collection].find()]


def get_number_of_pages(collection_name, query={}):
    return db[collection_name].count_documents(query) // PER_PAGES + 1


def get_phone(slug_name):
    phone = db[PHONES_COL_NAME].find_one({'s_name': slug_name})
    if phone:
        return phone
    else:
        raise NotFoundException


MAX_PRICE = 99999
PER_PAGES = 10


def get_phones(num_page=0, query={}):
    return list(db[PHONES_COL_NAME].find(query)
                .sort('rank', ASCENDING)
                .skip(num_page * PER_PAGES)
                .limit(PER_PAGES))


def get_screen_sizes():
    s = set()
    for phone in db[PHONES_COL_NAME].find():
        s.add(phone['screen']['diagonal'])
    return s
