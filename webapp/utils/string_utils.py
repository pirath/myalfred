import datetime
import json
import re

from bson import ObjectId
from flask import Response
from slugify import slugify


def closest(slug, s_list):
    slug_words = slug.split('-')
    match_scores = {}
    for item in s_list:
        splited_items = item.split('-')
        score = 0
        for word in slug_words:
            if word in splited_items:
                score += 1
            else:
                break

        if score > 0:
            match_scores[item] = score

    if match_scores:
        max_match_score = max(match_scores.values())
        results = [match_key for match_key, match_score in match_scores.items() if match_score == max_match_score]
        return max_match_score, min(results, key=len)
    else:
        return 0, None


class JSONDBEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'isoformat'):
            return obj.isoformat()
        if isinstance(obj, datetime.timedelta):
            return str(obj)
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, ObjectId):
            return str(obj)

        # return json.JSONEncoder.default(self, obj)


def json_response(data, status=200, indent=None, cls=JSONDBEncoder):
    return Response(response=json.dumps(data, indent=indent, cls=cls),
                    status=status,
                    mimetype="application/json")


def _remove_bracket(name):
    return re.sub(r'\([^)]*\)', '', name)


def get_simplified_name(name):
    name = _remove_bracket(name)
    return slugify(name.replace('+', 'plus'))