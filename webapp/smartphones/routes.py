# coding: utf-8

from flask import render_template, request

from webapp import app, database
from webapp.utils.string_utils import json_response


def _get_query_from_request(r):
    min_price = r.args.get('min-price')
    max_price = r.args.get('max-price')
    screen_size = r.args.get('screen-size')
    query = {}
    price_query = {}
    if min_price:
        price_query.update({'$gte': int(min_price)})
    if max_price:
        price_query.update({'$lte': int(max_price)})
    if price_query:
        query['price'] = price_query
    if screen_size:
        query['screen_size'] = {'$eq': float(screen_size)}
    return query


@app.route("/smartphones")
def get_smartphones():
    query = _get_query_from_request(request)
    num_page = int(request.args.get('num_page', 0))
    page_count = database.get_number_of_pages(database.PHONES_COL_NAME, query)
    return render_template(
        "smartphones/main.html",
        phones=database.get_phones(
            num_page=int(num_page) if num_page else 0,
            query=query
        ),
        page_count=page_count,
        current_page=num_page,
        screen_sizes=database.get_screen_sizes()
    )


@app.route("/smartphones/<s_name>", methods=['GET'])
def get_smartphone(s_name):
    return json_response(database.get_phone(s_name))
